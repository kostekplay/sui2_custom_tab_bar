////  SUI2_CustomTabBarApp.swift
//  SUI2_CustomTabBar
//
//  Created on 23/03/2021.
//  
//

import SwiftUI

@main
struct SUI2_CustomTabBarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
