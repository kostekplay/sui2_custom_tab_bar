////  ContentView.swift
//  SUI2_CustomTabBar
//
//  Created on 23/03/2021.
//  
//

import SwiftUI

struct ContentView: View {
    
    @State var selectedIndex = 0
    @State var presented = false
    
    let icons = ["house","gear","plus","lasso","message" ]
    
    var body: some View {
        VStack {
            
            ZStack {
                
                Spacer().fullScreenCover(isPresented: $presented, content: {
                    
                    Button(action: {
                        presented.toggle()
                    }, label: {
                        Text("New sheet.")
                            .frame(width: 300, height: 50, alignment: .center)
                            .foregroundColor(.white)
                            .background(Color(.blue))
                            .cornerRadius(10)
                            
                    })
                })
                
                switch selectedIndex {
                    
                    case 0:
                       HomeView()
                    case 1:
                        NavigationView {
                            VStack {
                                Text("First Screen")
                            }
                            .navigationTitle("Setting")
                        }
                        
                    case 2:
                        NavigationView {
                            VStack {
                                Text("First Screen")
                            }
                            .navigationTitle("Plus")
                        }
                        
                    case 3:
                        NavigationView {
                            VStack {
                                Text("First Screen")
                            }
                            .navigationTitle("Lasso")
                        }
                        
                    default:
                        NavigationView {
                            VStack {
                                Text("Second Screen")
                            }
                            .navigationTitle("Message")
                        }
                }
            }
            
            Spacer()
            
            Divider()
            
            HStack{
                
                ForEach(0..<5, id: \.self) { num in
                    
                    Spacer()
                    
                    Button(action: {
                        
                        selectedIndex = num
                        
                        if selectedIndex == 2 {
                            presented.toggle()
                        }

                    }, label: {
                        
                        if num == 2 {
                            Image(systemName: icons[num])
                                .font(.system(size: 25, weight: .regular, design: .default))
                                .foregroundColor(.white)
                                .frame(width: 50, height: 50, alignment: .center)
                                .background(Color(.blue))
                                .cornerRadius(25)
                        } else {
                            Image(systemName: icons[num])
                                .font(.system(size: 25, weight: .regular, design: .default))
                                .foregroundColor(selectedIndex == num ? .blue : .gray)
                        }
                    })
                    
                    Spacer()
                    
                }
            }
        }
    }
}

struct HomeView: View {
    var body: some View {
        NavigationView {
            VStack {
                Text("First Screen")
            }
            .navigationTitle("Home")
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
    }
}
